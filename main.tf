
resource "google_compute_disk" "disk-1" {
  count = "${var.node_count}"
  name = "disk-size-10-${count.index+1}"
  size = 10
  zone = "${var.current-zone}"
}

resource "google_compute_disk" "disk-2" {
  count = "${var.node_count}"
  name = "disk-size-8-${count.index+1}"
  size = 8
  zone = "${var.current-zone}"
}

resource "google_compute_instance" "my-servers" {
  count = "${var.node_count}"
  name         = "node${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      size = "20"
      image = "debian-cloud/debian-10"
    }
  }
  labels     = {
    "group" = "app-1",
  }
  attached_disk {
    source = google_compute_disk.disk-1["${count.index}"].name
    device_name = "disk1-node${count.index+1}"
  }
  attached_disk {
    source = google_compute_disk.disk-2["${count.index}"].name
    device_name = "disk2-node${count.index+1}"
  }

  metadata = {
    ssh-keys = "devopssam21:${file("~/.ssh/id_rsa.pub")}"
  }
  connection {
    type     = "ssh"
    user     = "devopssam21"
    private_key = "${file(var.private_key_path)}"
    host     = "${self.network_interface.0.access_config.0.nat_ip}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt install curl python3-pip  -y",
      "curl -sSL https://get.docker.com/ | sh",
      "sudo usermod -aG docker $(whoami)",
      "sudo curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose" 
    ]
  }
  provisioner "local-exec" {
   command = "TF_STATE=./terraform.tfstate ansible-playbook --inventory-file=deploy_app/gcp.yaml deploy_app/test.yml"
  }
  

  network_interface {
    network = "default"
    access_config {
    }
  }
}

output "hostname_projectid" {
   value = {
    for info in google_compute_instance.my-servers: 
    info.name => info.project
   }
}

