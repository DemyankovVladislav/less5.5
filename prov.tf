provider "google" {
  credentials = file("juneway-335712-51a94a0932a7.json")
  project     = "juneway-335712"
  region      = "europe-west3"
  zone        = "${var.current-zone}"
}
provider "google-beta" {
  credentials = file("juneway-335712-51a94a0932a7.json")
  region  = "europe-west3"
  project = "juneway-335712"
}


