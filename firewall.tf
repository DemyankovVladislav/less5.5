resource "google_compute_firewall" "http-https" {
  name = "http-https"
  network = "default"
  dynamic "allow" {
    for_each = [for s in var.firewall_ports : {
    protocol = "tcp"
    ports = s
    }]
    content {
     protocol = "tcp"
     ports = allow.value.ports[*]
    } 
  }
  source_ranges = ["0.0.0.0/0"]
}

